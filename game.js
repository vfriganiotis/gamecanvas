import Paddle from './paddle.js'
import Ball from './ball.js'
import Controller from './controller.js'
import Brick from "./brick.js"

let img = document.getElementById("brick");

export default class Game{

    constructor ( ctx,gameWidth,gameHeight , circles ) {

        this.ctx = ctx;

        this.gameWidth = gameWidth;
        this.gameHeight = gameHeight;
        this.paddle = new Paddle(this);
        this.controller = new Controller(this.paddle);
        this.ball = new Ball(550,350,10,this);
        this.brick = new Brick(img,0,0);
        this.bricksArr  = [
            [ 1 , 1 , 1, 1, 1 ,1 ,1 ,1],
            [ 1 , 1 , 1, 1, 1 ,1 ,1 ,1],
            [ 1 , 1 , 1, 1, 1, 1 ,1 ,1],
            [ 1 , 1 , 1, 1, 1, 1 ,1 ,1],
            [ 1 , 1 , 1, 1, 1, 1 ,1 ,1],
            [ 1 , 1 , 1, 1, 1, 1 ,1 ,1]
        ]
        this.circles = circles
        console.log(this.circles)

        this.chernobyll = 1

    }

    start(){

        this.ctx.clearRect( 0, 0, this.gameWidth, this.gameHeight);

        this.paddle.update()
        this.paddle.draw(this.ctx)

        this.ball.update()
        this.ball.draw(this.ctx)

        this.bricks(this.brick,this.ctx,this.bricksArr)

        this.renderCircles(this.circles)

    }

    renderCircles(circles) {
        //if (circles == {}) return false;
        //Loop al Circles
        for (var i = 0; i < circles.length; i++) {
            var circ = circles[i];
            //console.log(circ);
            this.ctx.beginPath();

            if(circ.speed === 0){
                this.ctx.arc(circ.gX ,circ.positionY , circ.size, 0, Math.PI * 2, false);
            }else{
                if( circ.direction === 'left' ){
                    circ.positionY = circ.positionY  + (circ.speed++ / 10) / 3
                    circ.positionX = circ.positionX  + (circ.speed++ / 10) / 10
                }else{
                    circ.positionY = circ.positionY  + (circ.speed++ / 10) / 3
                    circ.positionX = circ.positionX  - (circ.speed++ / 10) / 10
                }

                this.ctx.arc(circ.positionX , circ.positionY , circ.size, 0, Math.PI * 2, false);
            }

            this.ctx.fillStyle = circles[i].col;
            this.ctx.closePath();
            this.ctx.fill();

        }
    }

    bricks(brick,ctx,bricks){

        let buildX = 0;
        let buildY = 0;
        let ball = this.ball;
        let paddle = this.paddle;
        let circles = this.circles

        for( var i =0; i < circles.length; i++){

            paddle.guns.bulletArr.map(function(item,num){

                if( item.y < 0){
                    paddle.guns.remove(num)
                }

                let topOfGun = item.y;
                let leftOfGun = item.x;
                let rightOfGun = item.x + item.width;
                let bottomOfGun = item.y + item.height;

                let topOfObject = circles[i].gY;
                let leftSideOfObject = circles[i].gX;
                let rightSideOfObject = circles[i].gX + 20;
                let bottomOfObject = circles[i].gY + 20;

                if (
                    bottomOfGun >= topOfObject &&
                    topOfGun <= bottomOfObject &&
                    rightOfGun >= leftSideOfObject &&
                    leftOfGun <= rightSideOfObject
                ) {
                    circles[i].speed = 1
                }

            })
        }

        for( var i =0; i < bricks.length; i++){

            for( var y =0; y < bricks[i].length; y++){

                brick.positionX = buildX;

                paddle.guns.bulletArr.map(function(item,num){

                    if( item.y < 0){
                        paddle.guns.remove(num)
                    }

                    if( bricks[i][y] === 1 &&
                        item.x - item.width < buildX + 120 &&
                        item.x >= buildX &&
                        item.y + item.height - 60 < buildY &&
                        item.y - item.height >= buildY){

                        bricks[i][y] = 0
                        ball.speed.x = -ball.speed.x;
                        ball.speed.y = -ball.speed.y;

                    }

                    if( bricks[i][y] === 1 &&
                        item.x - item.width < buildX + 120 &&
                        item.x >= buildX &&
                        item.y + item.height - 60 < buildY &&
                        item.y - item.height >= buildY){

                        bricks[i][y] = 0
                        ball.speed.x = -ball.speed.x;
                        ball.speed.y = -ball.speed.y;

                    }

                })



                if( bricks[i][y] === 1 &&
                    ball.positionX - 10 < buildX + 120 &&
                    ball.positionX - 10 >= buildX &&
                    ball.positionY - 50 -10 < buildY &&
                    ball.positionY - 10 >= buildY){
                    bricks[i][y] = 0
                    ball.speed.x = -ball.speed.x;
                    ball.speed.y = -ball.speed.y;

                }

                if(bricks[i][y] === 1){

                    ctx.drawImage(img, buildX, buildY , 120 ,50 );
                    //item[x] = 0
                }

                buildX = buildX + 120

            }
            buildX = 0;
            buildY = buildY + 50;
        }

    }


}
