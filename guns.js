import Bullets  from "./bullets.js"

export default class Guns {

    constructor(paddle){
        this.paddle = paddle
        this.bulletArr = [new Bullets(this.paddle.positionX,this.paddle.positionY)]
    }

    fire(){
        this.bulletArr.push(new Bullets(this.paddle.positionX,this.paddle.positionY))
    }


    remove(item){
        this.bulletArr.splice(item,1)
        console.log(this.bulletArr)
    }

    draw(ctx){
        this.bulletArr.map(function(item){
            item.y--
            ctx.fillRect(item.x , item.y , item.width, item.height)
        })
    }
}