import Guns from "./guns.js"

export default class Paddle {

     constructor(game){
         this.width = 150;
         this.height = 10;
         this.positionX = game.gameWidth/2 - this.width/2;
         this.positionY = game.gameHeight - this.height  - 70;
         this.speed = 0;
         this.maxSpeed = 7;
         this.gameWidth = game.gameWidth;
         this.gameHeight = game.gameHeight;
         this.guns = new Guns(this)
     }

     draw(ctx) {
         ctx.fillStyle = 'red';
         ctx.fillRect(this.positionX, this.positionY, this.width, this.height)
         this.guns.draw(ctx)
     }

     fire(){
         this.guns.fire()
     }

     moveLeft(){
         this.speed = -this.maxSpeed;
     }

     moveRight(){
         this.speed = this.maxSpeed;
     }

     stop(){
         this.speed = 0;
     }

     update() {
        this.positionX += this.speed;
        if (this.positionX < 0) this.positionX = 0;
        if (this.positionX + this.width > this.gameWidth)
            this.positionX = this.gameWidth - this.width;
     }

}