import Game from './game.js'

let canvas = document.getElementById('GAME')
let ctx = canvas.getContext('2d')

function wrapText(context, TEXT, x, y, maxWidth, lineHeight) {

    var words = TEXT.split(' ');
    var line = '';

    for(var n = 0; n < words.length; n++) {
        var testLine = line + words[n] + ' ';
        var metrics = context.measureText(testLine);
        var testWidth = metrics.width;
        if (testWidth > maxWidth && n > 0) {
            context.textBaseline = 'middle';
            context.textAlign = "center";
            context.fillText(line, x + 200, y);
            line = words[n] + ' ';
            y += lineHeight;
        }
        else {
            line = testLine;
        }
    }

    context.textBaseline = 'middle';
    context.textAlign = "center";
    context.fillText(line , x + 200, y);

}

const canvasWIDTH = 800;
const canvasHEIGHT = 600;

let lastime = 0;

//ctx,gameWidth,gameHeight,paddle,ball,controller

var bgCanvas = document.createElement('canvas');
bgCanvas.width = 800;
bgCanvas.height = 600;
var bgCtx = bgCanvas.getContext('2d');

var maxWidth = 400;
var lineHeight = 20;
var x = (canvasWIDTH - maxWidth) / 2;
var y = 60;
//var textR = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).';
var textR = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.op publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident';

bgCtx.font = '25px Calibri';
bgCtx.fillStyle = '#000';

ctx.font = '25px Calibri';
ctx.fillStyle = '#000';

wrapText(bgCtx, textR, x, y, maxWidth, lineHeight);

//Circles Array
var circles = [];
var pixel,imageData,width,height

var densess = 12;

imageData = bgCtx.getImageData(0,0,bgCanvas.width,bgCanvas.height)

function init() {
    for (height = 0; height < bgCanvas.height; height += densess) {
        for (width = 0; width < bgCanvas.width; width += densess) {
            //We get the PIXEL DATA
            pixel = imageData.data[(width + height * bgCanvas.width) * 4 - 1];
            //And Also Check for the Pixel Value and Render a Circle of true on the init function
            if (pixel > 128) {
                drawCircle(width, height); ///< we draw on each pixel
            }
        }
    }
}

function drawCircle(x, y) {


    let dir = 'left';
    if(Math.round(Math.random()) === 1){
        dir = 'right';
    }
    circles.push({
        gX: x, ///< Goal X
        gY: y, ///< Goal Y
        size:5,
        speed: 0,
        positionY: y,
        positionX: x,
        direction:  dir,
        col: "yellow", //< a custom color for all the circles
    });
}

init();

let game = new Game(ctx,canvasWIDTH,canvasHEIGHT,circles)

function paint(timestamp){

    let time = timestamp - lastime;

    lastime = timestamp;
    game.start()

    wrapText(ctx, textR, x, y, maxWidth, lineHeight);
    // renderCircles(circles);

    //update
    requestAnimationFrame(paint)

}

paint()
