export default class Controller {

   constructor (paddle){

       document.addEventListener('keydown', function (event) {

           switch(event.keyCode){
               case 65 :
                   paddle.moveLeft()
                 break;
               case 68 :
                   paddle.moveRight()
                 break;
               case 69 :
                   paddle.fire()
                   console.log('e')
                   console.log(paddle)
                   break;
               case 81 :
                   paddle.fire()
                   break;
           }

       })

       document.addEventListener('keyup', function (event) {

           switch(event.keyCode){
               case 65 :
                   if(paddle.speed < 0)
                       paddle.stop()
                   break;
               case 68 :
                   if(paddle.speed > 0)
                       paddle.stop()
                   break;
           }

       })
   }

}